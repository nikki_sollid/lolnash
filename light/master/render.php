<?php
class render {
    /*
    private function model($model)
    {
        require_once 'models/' . $model . '.php';
        return new $model();
    }
    */
   /**
    * Retrieve URL for image by filename
    * @param string $filename
    * @return string
    */
	public static function getImg($filename){
		return "/assets/img/" . $filename;
	}

   /**
    * Retrieve URL for CSS by filename
    * @param string $filename
    * @return string
    */
    public static function getCss($filename){
		return HTTP . "assets/css/" . $filename;
	}

   /**
    * Retrieve URL for JavaScript by filename
    * @param string $filename
    * @return string
    */
	public static function getJs($filename){
		return HTTP . "assets/js/" . $filename;
	}

   /**
    * Escape potentially harmful characters from string
    * @param string $string
    * @return string
    */
    public static function escape($string) {
        return htmlentities($string, ENT_QUOTES, 'UTF-8');
    }

   /**
    * Flash a one time message to user, using _SESSION
    * @param string $type
    * @param string $message
    * @return void
    */
    public static function flash($type, $message = "") {
        switch ($type) {
            case 'success':
                $classes = "alert alert-dismissible alert-success";
                break;

            case 'info':
                $classes = "alert alert-dismissible alert-info";
                break;

            case 'warning':
                $classes = "alert alert-dismissible alert-warning";
                break;

            case 'danger':
                $classes = "alert alert-dismissible alert-danger";
                break;

            default:
                $classes = "alert alert-dismissible alert-info";
                break;
        }
        if (Session::exists($type)) {
            $string = Session::get($type);
            Session::delete($type);
            echo '<div class="' . $classes . '" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>'
                        . $string .
                    '</div>';
        } else {
            Session::put($type, $message);
        }
    }
}
