<?php
class view {

   /**
    * Renders view with optional data associated
    * @param string $view
    * @param mixed $data
    * @return void
    */
    public static function render($view, $data = [])
    {
        require_once '../views/' . $view . '.php';
    }
}
 ?>
