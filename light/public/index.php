<?php
include_once "../master/functions.php";
spl_autoload_register(function($class) {
   require_once '../master/' . $class . '.php';
});

if(isset($_GET['url'])){
$url = $_GET['url'];
$url = explode('/',$url);
unset($url[0]);
$base_url = $url[1];
}
else{
  $url = "";
}

$method = "index";
if(!isset($url[1])){
  $base_url = "home";
  $url[1] = "home";
}

if(file_exists("../controller/".$base_url.".php")){
  require_once "../controller/" . $url[1] . ".php";
  $controller = new $url[1];
  unset($url[1]);
}else{
  require_once "../controller/error404.php";
  $controller = new error404;
}
if(isset($url[2])){
if(method_exists($controller,$url[2])){
  $method = $url[2];
  unset($url[2]);
}
}
  $params = $url ? array_values($url) : [];
  call_user_func_array([$controller, $method],$params);

 ?>
