
function objectifyString(s){
	var result =  JSON.parse(s);
	console.log(result);
	return result;
}
function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function getData(){
	/* SETTINGS */
	window.location = window.location.href + "/result/";
	var name = document.getElementById("mainField").value;
	var script_type = "summoner";//$( "#script option:selected" ).val();
	var api_key = "b48aa6b5-c293-4577-8aa0-5cff365cde25";
	var server = $( "#server option:selected" ).val();

	/* SCRIPT TYPES */
	var scripts = Array();
	scripts["rank"] = "https://euw.api.pvp.net/api/lol/euw/v2.5/league/master?type=RANKED_SOLO_5x5&api_key="+api_key;
	scripts["summoner"] = "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/"+name+"?api_key="+api_key;

	/* RESULT */
	var result = objectifyString(httpGet(scripts[script_type]));
	if(typeof result === 'object'){
		console.log("Javascript object Parsed.");
	}else{
		console.log("Something went wrong.");
	}
}
